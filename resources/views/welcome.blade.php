{{-- Activity for s02 

@extends('layouts.app')

@section('content')

  <div> 
    <img class="img-fluid" src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png">
    <h3 class="mt-5 text-center">FEATURED POSTS:</h3>
  </div>

  @if(count($posts) > 0)
    @foreach($posts->random(3) as $post)
      <div class="card text-center m-3">
          <div class="card-body">
            <h3 class="card-title">
                <a href="/posts/{{$post->id}}">{{$post->title}}</a>
            </h4>
            <p class="card-subtitle"> Author: {{$post->user->name}} </p>
          </div>
      </div>      
    @endforeach   
  @else
    <div class="text-center mt-4">
      <h4>There are no posts to show!</h4>
    </div>
  @endif

@endsection 
 --}}
{{-- random(3), will result to error if post count > 3 // requested 3 items, but only 1/2 items available.--}}


{{-- alternative --}}

@extends('layouts.app')

@section('content')
  <div class="text-center">
    <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" class="img-fluid w-50">
    <h2 class="my-4">Featured Post</h2>

    @if(count($posts) > 0)
      @foreach($posts as $post)
        <div class="card mt-3">
          <div class="card-body">
            <h4 class="card-title mb-3">
              <a href="/posts/{{$post->id}}">{{$post->title}}</a>
            </h4>
            <h6 class="card mb-3"> Author: {{$post->user->name}}</h6>
          </div>
        </div>
      @endforeach
    @else
      <div>
        <h2>There are no posts to show!</h2>
      </div>
    @endif

  </div>
@endsection

