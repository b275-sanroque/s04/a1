<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// to access with the authenticated user
use Illuminate\Support\Facades\Auth;
// To have access with queries related to the Post Entity/Model.
use App\Models\Post;

class PostController extends Controller
{
    //action to return a view containing a form for post creation
    public function create(){
        return view('posts.create');
    }

    // action to received the form data and subsequently store said data in the post table
    public function store(Request $request){
        // if there is an aunthenticated user
        if(Auth::user()){
            // instantiate a new post object from the Post model
            $post = new Post;
            // define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key (user_id)
            $post->user_id = (Auth::user()->id);
            // save this post object in the database
            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    // action that will return a view showing all blog posts
    public function index(){
        // $posts = Post::all();
         $posts = Post::where('isActive', true)->get();
        // The "with()" method will allows us to pass information from the controller to view page
        return view('posts.index')->with('posts', $posts);
    }

    // Activity for s02
    public function welcome(){
        // $posts = Post::all();
         $posts = Post::where('isActive', true)->get();

        //alternative
        $posts = Post::inRandomOrder()
                 ->limit(3)
                 ->get();

        return view('welcome')->with('posts', $posts);
    }

    // Discussion s03
    // action to show only the post authored by the authenticated user.
    public function myPost() {
        if(Auth::user()){
            // we are able to fetch the posts related to a specific user because of the established relationship between the models.
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        } else{
            return redirect('/login');
        }
    }

    // action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown.
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    // Activity for s03
    // action that will return view showing edit form
    public function edit($id){
        $post = Post::find($id);
        if(Auth::user()) {
          if(Auth::user()->id == $post->user_id){
              return view('posts.edit')->with('post', $post);
          } else{
              return redirect('/posts');
          }
        } else{
            return redirect('/login');
        }
    }

    //Discussion s04
    // action for updating edited post
    public function update(Request $request, $id){
        $post = Post::find($id);
        // added validation, if authenticated user's ID is the same with the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        // return view('posts.show')->with('post', $post);
        return redirect('/posts');
    }

    // action to remve a post with the matching URL id parameter
     // public function destroy($id){
     //     $post = Post::find($id);
     //     // 
     //     if(Auth::user()->id == $post->user_id){
     //         $post->delete();
     //     }
     //     return redirect('/posts');
     // }
    
    public function archive($id){
        $post = Post::find($id);

        if(Auth::user()) {
          if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            $post->save();
          }
        }         
        return redirect('/posts');
    }

}
